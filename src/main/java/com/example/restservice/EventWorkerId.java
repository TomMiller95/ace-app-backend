package com.example.restservice;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EventWorkerId implements Serializable {

	@Column(name = "event_id")
	private Integer eventId;

	@Column(name = "user_id")
	private Integer userId;
	
	public EventWorkerId() {
	}

	public EventWorkerId(Integer eventId, Integer userId) {
		this.eventId = eventId;
		this.userId = userId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public Integer getUserId() {
		return userId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		EventWorkerId that = (EventWorkerId) o;
		return Objects.equals(eventId, that.eventId) && Objects.equals(userId, that.userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(eventId, userId);
	}
}
