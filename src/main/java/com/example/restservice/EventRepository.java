package com.example.restservice;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer>{	
	@Query(value = "SELECT e FROM Event e WHERE e.id = :id AND e.eventDate = :todaysDate")
	Iterable<Event> getTodaysEvents(@Param("id") int id, @Param("todaysDate") String todaysDate);
}