package com.example.restservice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventController {

	@Autowired
	EventRepository eventRepo;
	@Autowired
	UserRepository userRepo;

	@GetMapping("/addEvent")
	public Event addEvent(@RequestParam(value = "startTime", defaultValue = "") String startTime,
			@RequestParam(value = "endTime", defaultValue = "") String endTime,
			@RequestParam(value = "state", defaultValue = "") String state,
			@RequestParam(value = "eventDesc", defaultValue = "") String eventDesc,
			@RequestParam(value = "eventDate", defaultValue = "") String eventDate,
			@RequestParam(value = "numOss", defaultValue = "") String numOss,
			@RequestParam(value = "numAoss", defaultValue = "") String numAoss,
			@RequestParam(value = "numBackups", defaultValue = "") String numBackups) {

		Event newEvent = new Event();
		newEvent.setStartTime(startTime);
		newEvent.setEndTime(endTime);
		newEvent.setState(state);
		newEvent.setEventDesc(eventDesc);
		newEvent.setEventDate(eventDate);
		newEvent.setNumOssNeeded(numOss);
		newEvent.setNumAossNeeded(numAoss);
		newEvent.setNumBackupsNeeded(numBackups);
		eventRepo.save(newEvent);
		return newEvent;
	}

	@GetMapping(path = "/loadUsersEvents")
	public @ResponseBody Iterable<EventDTO> getUsersEvents(
			@RequestParam(value = "userId", defaultValue = "") String id) {
		UserDTO u = new UserDTO(userRepo.getById(Integer.parseInt(id)), true);
		return u.getEvents();
	}

	@GetMapping(path = "/loadTodaysEvents")
	public @ResponseBody Iterable<EventDTO> getTodaysEvents(
			@RequestParam(value = "userId", defaultValue = "") String id,
			@RequestParam(value = "todaysDate", defaultValue = "") String todaysDate) {
		UserDTO u = new UserDTO(userRepo.getById(Integer.parseInt(id)), true);

//		List<Event> todaysEvents = (List<Event>) eventRepo.getTodaysEvents(Integer.parseInt(id), todaysDate);
//		
//		List<EventDTO> dtoEvents = new ArrayList();
//		for (int i = 0; i < todaysEvents.size(); i++) {
//			dtoEvents.add(new EventDTO(todaysEvents.get(i), false));
//		}

		List<EventDTO> allUserEvents = u.getEvents();
		List<EventDTO> todaysEvents = new ArrayList();
		for (int i = 0; i < allUserEvents.size(); i++) {
			if (allUserEvents.get(i).getEventDate().equals(todaysDate)) {
				todaysEvents.add(allUserEvents.get(i));
			}
		}
		return todaysEvents;
	}

	@GetMapping(path = "/loadAllEvents")
	public @ResponseBody Iterable<EventDTO> getAllEvents() {
		List<Event> allEvents = eventRepo.findAll();
		List<EventDTO> dtoEvents = new ArrayList();
		for (int i = 0; i < allEvents.size(); i++) {
			dtoEvents.add(new EventDTO(allEvents.get(i), true));
		}
		boolean use75 = false;
		if (use75) {
			double maysLandingLat = 39.457970;
			double maysLandingLong = -74.651270;
			double mortonLat = 39.466820;
			double mortonLong = -75.125890;
			System.out.println(DistanceCalculator.distance(maysLandingLat, maysLandingLong, mortonLat, mortonLong, "M") + " Miles\n");
			System.out.println(DistanceCalculator.distance(maysLandingLat, maysLandingLong, mortonLat, mortonLong, "K") + " Kilometers\n");
			System.out.println(DistanceCalculator.distance(maysLandingLat, maysLandingLong, mortonLat, mortonLong, "N") + " Nautical Miles\n");
			
//			DistanceCalculator d = new DistanceCalculator();
//			d.getDistance(maysLandingLat, maysLandingLong, mortonLat, mortonLong);
		}
		return dtoEvents;
	}

	@PostMapping(path = "/setAvailable")
	public @ResponseBody void setAvailable(@RequestParam(value = "isAvailable", defaultValue = "") String isAvailable,
			@RequestParam(value = "userId", defaultValue = "") String userId,
			@RequestParam(value = "eventId", defaultValue = "") String eventId) {
		UserDTO userDTO = new UserDTO(userRepo.getById(Integer.parseInt(userId)), true);
		EventDTO eventDTO = new EventDTO(eventRepo.getById(Integer.parseInt(eventId)), true);
		if (isAvailable.equals("True")) {
			if (userDTO.getEvents().contains(eventDTO)) {
				System.out.println("User was already in here. This should not be possible.");
			} else if (hasConflictingTimes(userDTO, eventDTO)) {
				System.out.println("User has a conflicting event at this time. Allow this to go through, "
						+ "but when Kelly approves then it should notify her as well.");
				userDTO.setStatus("Available");
				userDTO.addEvent(eventDTO);
				eventDTO.addUser(userDTO);
			} else {
				userDTO.setStatus("Available");
				userDTO.addEvent(eventDTO);
				eventDTO.addUser(userDTO);
			}
		} else {
			userDTO.removeEvent(eventDTO);
			eventDTO.removeUser(userDTO);
		}
		User saveUser = new User(userDTO, true);
		Event saveEvent = new Event(eventDTO, true);
		userRepo.save(saveUser);
		eventRepo.save(saveEvent);
	}

	// TODO make sure to compare the date as well
	private boolean hasConflictingTimes(UserDTO user, EventDTO event) {
		// TODO CN1 might not like the replace method.
		String eventStartTime = event.getStartTime().replace(":", "");
		String eventEndTime = event.getEndTime().replace(":", "");
		int eventStartTimeInt = 0;
		int eventEndTimeInt = 0;
		if (eventStartTime.contains("PM")) {
			eventStartTime = eventStartTime.replace("PM", "").trim();
			eventStartTimeInt = Integer.parseInt(eventStartTime);
			eventStartTimeInt += 1200;
			if (eventStartTimeInt == 2400) {
				eventStartTimeInt = 1200;
			}
		} else {
			eventStartTime = eventStartTime.replace("AM", "").trim();
			eventStartTimeInt = Integer.parseInt(eventStartTime);
			if (eventStartTimeInt >= 1200 && eventStartTimeInt < 100) {
				eventStartTimeInt -= 1200;
			}
		}
		if (eventEndTime.contains("PM")) {
			eventEndTime = eventEndTime.replace("PM", "").trim();
			eventEndTimeInt = Integer.parseInt(eventEndTime);
			eventEndTimeInt += 1200;
			if (eventEndTimeInt == 2400) {
				eventEndTimeInt = 1200;
			}
		} else {
			eventEndTime = eventEndTime.replace("AM", "").trim();
			eventEndTimeInt = Integer.parseInt(eventEndTime);
		}

		List<EventDTO> events = user.getEvents();
		for (int i = 0; i < events.size(); i++) {
			EventDTO currEvent = events.get(i);
			if (event.getId() != currEvent.getId()) {
				String currEventStartTime = currEvent.getStartTime().replace(":", "");
				String currEventEndTime = currEvent.getEndTime().replace(":", "");
				int currEventStartTimeInt = 0;
				int currEventEndTimeInt = 0;
				if (currEventStartTime.contains("PM")) {
					currEventStartTime = currEventStartTime.replace("PM", "").trim();
					currEventStartTimeInt = Integer.parseInt(currEventStartTime);
					currEventStartTimeInt += 1200;
					if (currEventStartTimeInt == 2400) {
						currEventStartTimeInt = 1200;
					}
				} else {
					currEventStartTime = currEventStartTime.replace("AM", "").trim();
					currEventStartTimeInt = Integer.parseInt(currEventStartTime);
					if (currEventStartTimeInt >= 1200 && currEventStartTimeInt < 100) {
						currEventStartTimeInt -= 1200;
					}
				}
				if (currEventEndTime.contains("PM")) {
					currEventEndTime = currEventEndTime.replace("PM", "").trim();
					currEventEndTimeInt = Integer.parseInt(currEventEndTime);
					currEventEndTimeInt += 1200;
					if (currEventEndTimeInt == 2400) {
						currEventEndTimeInt = 1200;
					}
				} else {
					currEventEndTime = currEventEndTime.replace("AM", "").trim();
					currEventEndTimeInt = Integer.parseInt(currEventEndTime);
					if (currEventEndTimeInt >= 1200 && currEventEndTimeInt < 100) {
						currEventEndTimeInt -= 1200;
					}
				}
				if (eventStartTimeInt >= currEventStartTimeInt && eventStartTimeInt <= currEventEndTimeInt) {
					return true;
				} else if (eventEndTimeInt >= currEventStartTimeInt && eventEndTimeInt <= currEventEndTimeInt) {
					return true;
				} else if (currEventStartTimeInt >= eventStartTimeInt && currEventStartTimeInt <= eventEndTimeInt) {
					return true;
				} else if (currEventEndTimeInt >= eventStartTimeInt && currEventEndTimeInt <= eventEndTimeInt) {
					return true;
				}
			}
		}
		return false;
	}
}
