package com.example.restservice;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class EventDTO {

	private Integer id;
	private String startTime;
	private String endTime;
	private String state;
	private String eventDesc;
	private String eventDate;
	private String numOssNeeded;
	private String numAossNeeded;
	private String numBackupsNeeded;
	private List<UserDTO> users = new ArrayList();

	public EventDTO() {
	}
	
	public EventDTO(Event e, boolean loadData) {
		this.id = e.getId();
		this.startTime = e.getStartTime();
		this.endTime = e.getEndTime();
		this.state = e.getState();
		this.eventDesc = e.getEventDesc();
		this.eventDate = e.getEventDate();
		this.numOssNeeded = e.getNumOssNeeded();
		this.numAossNeeded = e.getNumAossNeeded();
		this.numBackupsNeeded = e.getNumBackupsNeeded();
		if (loadData) {
			for (EventWorker eventWorker : (List<EventWorker>) e.getUsers()) {
				User user = eventWorker.getUser();
				UserDTO userDTO = new UserDTO();
				userDTO.setId(user.getId());
				userDTO.setUsername(user.getUsername());
				userDTO.setState(user.getState());
				userDTO.setStatus(eventWorker.getStatus());
				userDTO.setAccessLevel(user.getAccessLevel());
				List<EventDTO> eventDTOs = new ArrayList();
				for (int i = 0; i < user.getEvents().size(); i++) {
					EventWorker ew = (EventWorker)user.getEvents().get(i);
					eventDTOs.add(new EventDTO(ew.getEvent(), false));
				}
				userDTO.setEvents(eventDTOs);
				users.add(userDTO);
			}
		}
	}
	
	public void addUser(UserDTO u) {
		users.add(u);
	}

	public void removeUser(UserDTO u) {
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getId() == u.getId()) {
				users.remove(users.get(i));
			}
		}
	}
}