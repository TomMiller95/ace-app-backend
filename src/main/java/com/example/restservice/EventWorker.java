package com.example.restservice;

import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity(name = "EventWorkers")
@Table(name = "event_Workers")
public class EventWorker {

	@EmbeddedId
	private EventWorkerId id;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("eventId")
	private Event event;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("userId")
	private User user;
	
	private String status = "";

	public EventWorker() {
	}

	public EventWorker(Event e, User u) {
		event = e;
		user = u;
		this.id = new EventWorkerId(e.getId(), u.getId());
	}

	public Event getEvent() {
		return event;
	}

	public User getUser() {
		return user;
	}

	public void setEvent(Event e) {
		event = e;
	}

	public void setUser(User u) {
		user = u;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String s) {
		status = s;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		EventWorker that = (EventWorker) o;
		return Objects.equals(user, that.user) && Objects.equals(event, that.event);
	}

	@Override
	public int hashCode() {
		return Objects.hash(event, user);
	}
}
