package com.example.restservice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.NaturalIdCache;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "Event")
@Table(name = "event")
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String startTime;
	private String endTime;
	private String state;
	private String eventDesc;
	private String eventDate;
	private String numOssNeeded;
	private String numAossNeeded;
	private String numBackupsNeeded;

	@JsonIgnore
	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EventWorker> users = new ArrayList<>();

	public Event() {

	}

	public Event(Integer id, String startTime, String endTime, String state, String eventDesc, String eventDate) {
	}
	
	public Event(EventDTO e, boolean loadData) {
		this.id = e.getId();
		this.startTime = e.getStartTime();
		this.endTime = e.getEndTime();
		this.state = e.getState();
		this.eventDesc = e.getEventDesc();
		this.eventDate = e.getEventDate();
		this.numOssNeeded = e.getNumOssNeeded();
		this.numAossNeeded = e.getNumAossNeeded();
		this.numBackupsNeeded = e.getNumBackupsNeeded();
		if (loadData) {
			for (UserDTO userDTO : (List<UserDTO>) e.getUsers()) {
				User u = new User(userDTO, false);
				EventWorker ew = new EventWorker(this, u);
				ew.setStatus(userDTO.getStatus());
				users.add(ew);
			}
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	public void setNumOssNeeded(String v) {
		numOssNeeded = v;
	}	
	public void setNumAossNeeded(String v) {
		numAossNeeded = v;
	}	
	public void setNumBackupsNeeded(String v) {
		numBackupsNeeded = v;
	}

	public String getNumOssNeeded() {
		return numOssNeeded;
	}
	public String getNumAossNeeded() {
		return numAossNeeded;
	}
	public String getNumBackupsNeeded() {
		return numBackupsNeeded;
	}

	public List getUsers() {
		return users;
	}

	public void setUsers(List users) {
		this.users = users;
	}

	public void addUser(User user) {
		EventWorker eventWorker = new EventWorker(this, user);
		users.add(eventWorker);
		user.getEvents().add(eventWorker);
	}

	public void removeUser(User user) {
		for (Iterator<EventWorker> iterator = users.iterator(); iterator.hasNext();) {
			EventWorker eventWorker = iterator.next();

			if (eventWorker.getEvent().equals(this) && eventWorker.getUser().equals(user)) {
				iterator.remove();
				eventWorker.getUser().getEvents().remove(eventWorker);
				eventWorker.setEvent(null);
				eventWorker.setUser(null);
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Event event = (Event) o;
		return Objects.equals(id, event.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}