package com.example.restservice;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import lombok.Data;

@Data
public class UserDTO {

	private Integer id;
	private String username;
	private String password;
	private String state;
	private String accessLevel; // K = Kelly(Admin), M = Manager, C = Contractor
	private List<EventDTO> events = new ArrayList();
	
	@Transient
	private String status; // Status of an event. Linked to eventWorker

	public UserDTO() {
	}

	public UserDTO(User u, boolean loadData) {
		this.id = u.getId();
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.state = u.getState();
		this.accessLevel = u.getAccessLevel();
		if (loadData) {
			for (EventWorker eventWorker : (List<EventWorker>) u.getEvents()) {
				Event event = eventWorker.getEvent();
				EventDTO eventDTO = new EventDTO();
				eventDTO.setId(event.getId());
				eventDTO.setStartTime(event.getStartTime());
				eventDTO.setEndTime(event.getEndTime());
				eventDTO.setEventDate(event.getEventDate());
				eventDTO.setEventDesc(event.getEventDesc());
				eventDTO.setState(event.getState());
				eventDTO.setNumOssNeeded(event.getNumOssNeeded());
				eventDTO.setNumAossNeeded(event.getNumAossNeeded());
				eventDTO.setNumBackupsNeeded(event.getNumBackupsNeeded());
				List<UserDTO> userDTOs = new ArrayList();
				for (int i = 0; i < event.getUsers().size(); i++) {
					EventWorker ew = (EventWorker)event.getUsers().get(i);
					UserDTO userDTO = new UserDTO(ew.getUser(), false);
					userDTO.setStatus(eventWorker.getStatus());
					userDTOs.add(userDTO);
				}
				eventDTO.setUsers(userDTOs);
				events.add(eventDTO);
			}
		}
	}
	
	public List getEvents() {
		return events;
	}
	
	public void addEvent(EventDTO e) {
		events.add(e);
	}

	public void removeEvent(EventDTO e) {
		for (int i = 0; i < events.size(); i++) {
			if (events.get(i).getId() == e.getId()) {
				events.remove(events.get(i));
			}
		}
	}
}
