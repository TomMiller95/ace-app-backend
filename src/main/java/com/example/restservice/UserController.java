package com.example.restservice;

import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepo;

	@GetMapping("/login")
	public UserDTO login(@RequestParam(value = "userName", defaultValue = "") String userName,
			@RequestParam(value = "password", defaultValue = "") String pass) {
		User user = new User();
		user = userRepo.findUserWithUserPassword(userName, pass);
		UserDTO userDTO = new UserDTO(user, true);
		return userDTO;
	}

	@GetMapping("/addUser")
	public User addUser(@RequestParam(value = "userName", defaultValue = "") String userName,
			@RequestParam(value = "password", defaultValue = "") String pass,
			@RequestParam(value = "state", defaultValue = "") String state,
			@RequestParam(value = "accessLevel", defaultValue = "") String accessLevel) {

		User newUser = new User();
		newUser.setUsername(userName);
		newUser.setPassword(pass);
		newUser.setState(state);
		newUser.setAccessLevel(accessLevel);
		userRepo.save(newUser);
		return newUser;
	}
}