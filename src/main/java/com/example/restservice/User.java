package com.example.restservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "User")
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String username;
	private String password;
	private String state;
	private String accessLevel; // K = Kelly(Admin), M = Manager, C = Contractor
		
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EventWorker> events = new ArrayList<>();

	public User() {
	}

	public User(Integer id, String username, String password, String state) {
	}
	
	public User(UserDTO u, boolean loadData) {
		this.id = u.getId();
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.state = u.getState();
		this.accessLevel = u.getAccessLevel();
		if (loadData) {
			for (EventDTO eventDTO : (List<EventDTO>) u.getEvents()) {
				Event e = new Event(eventDTO, false);
				EventWorker ew = new EventWorker(e, this);
				ew.setStatus(u.getStatus());
				events.add(ew);
			}
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAccessLevel() {
		return accessLevel;
	}
	
	public void setAccessLevel(String v) {
		accessLevel = v;
	}
	
	public List getEvents() {
		return events;
	}

	public void setEvents(List events) {
		this.events = events;
	}

	public void addEvent(Event event) {
		EventWorker eventWorker = new EventWorker(event, this);
		events.add(eventWorker);
		event.getUsers().add(eventWorker);
	}

	public void removeEvent(Event event) {
		for (Iterator<EventWorker> iterator = events.iterator(); iterator.hasNext();) {
			EventWorker eventWorker = iterator.next();

			if (eventWorker.getUser().equals(this) && eventWorker.getEvent().equals(event)) {
				iterator.remove();
				eventWorker.getEvent().getUsers().remove(eventWorker);
				eventWorker.setEvent(null);
				eventWorker.setUser(null);
			}
		}
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        User user = (User) o;
        return Objects.equals(id, user.id);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}